package com.lld.problems.rideSharing2.models;

public class Ride {
    private final static int AMOUNT_PER_KM = 20;
    private final int id;
    private int origin;
    private int destination;
    private int costOfRide;
    private int numberOfSeats;
    private RideStatus rideStatus;
    private final Driver driver;

    public void setOrigin(int origin) {
        this.origin = origin;
    }

    public void setDestination(int destination) {
        this.destination = destination;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public Ride(int id, int origin, int destination, int numberOfSeats, Driver driver) {
        this.id = id;
        this.origin = origin;
        this.destination = destination;
        this.numberOfSeats = numberOfSeats;
        this.driver = driver;
        this.rideStatus = RideStatus.CREATED;
        this.costOfRide = 0;
    }

    public void setRideStatus(RideStatus rideStatus) {
        this.rideStatus = rideStatus;
    }

    public int getId() {
        return id;
    }

    public int getOrigin() {
        return origin;
    }

    public int getDestination() {
        return destination;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public RideStatus getRideStatus() {
        return rideStatus;
    }

    public int getCostOfRide(boolean isPrivilegedUser) {
        int cost = 0;
        if(numberOfSeats<2) {
            cost = (int) (AMOUNT_PER_KM * Math.abs(origin-destination) * (isPrivilegedUser? 0.75:1));
        } else {
            cost = (int) (AMOUNT_PER_KM * Math.abs(origin-destination) * numberOfSeats *(isPrivilegedUser? 0.5:0.75));
        }
        this.costOfRide = cost;
        return this.costOfRide;
    }

    public Driver getDriver() {
        return driver;
    }
}
