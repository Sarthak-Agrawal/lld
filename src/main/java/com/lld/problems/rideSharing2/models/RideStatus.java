package com.lld.problems.rideSharing2.models;

public enum RideStatus {
    CREATED,
    IN_PROGRESS,
    WITHDRAWN,
    COMPLETED
}
