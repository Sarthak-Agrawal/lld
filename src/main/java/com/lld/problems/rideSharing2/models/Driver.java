package com.lld.problems.rideSharing2.models;

import java.util.ArrayList;

public class Driver extends Person {

    private final ArrayList<Ride> pastRides;
    private Ride currentRide;

    public Driver(String name) {
        this.pastRides = new ArrayList<>();
        this.currentRide = null;
        this.name = name;
    }

    public void setCurrentRide(Ride currentRide) {
        if(this.currentRide!=null) {
            pastRides.add(this.currentRide);
        }
        this.currentRide = currentRide;
    }

    public ArrayList<Ride> getPastRides() {
        return pastRides;
    }

    public Ride getCurrentRide() {
        return currentRide;
    }
}
