package com.lld.problems.rideSharing2.models;

import java.util.HashSet;

public class Rider extends Person {
    private boolean isPrivileged;
    private final HashSet<Integer> pastRides;
    private Ride currentRide;

    public Rider(String name) {
        this.name = name;
        pastRides = new HashSet<>();
        currentRide = null;
        this.isPrivileged = false;
    }

    public HashSet<Integer> getPastRides() {
        return pastRides;
    }

    public Ride getCurrentRide() {
        return currentRide;
    }

    public void setCurrentRide(Ride currentRide) {
        this.currentRide = currentRide;
    }

    public boolean isPrivileged() {
        return isPrivileged;
    }

    public void setPrivileged(boolean privileged) {
        isPrivileged = privileged;
    }
}
