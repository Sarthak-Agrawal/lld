package com.lld.problems.rideSharing2.dao;

import com.lld.problems.rideSharing2.models.Rider;

import java.util.HashMap;

public class Riders {
    private final HashMap<String, Rider> riders;

    public Riders() {
        riders = new HashMap<>();
    }

    public void addRider(Rider rider) {
        if (riders.containsKey(rider.getName())) {
            System.out.println("Rider already exists");
            return;
        }
        riders.put(rider.getName(), rider);
    }

    public Rider getRider(String name) {
        if(!riders.containsKey(name)) {
            //Throw error
            System.out.println("Rider does not exist");
            return null;
        }

        return riders.get(name);
    }
}
