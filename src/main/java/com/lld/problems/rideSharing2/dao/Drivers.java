package com.lld.problems.rideSharing2.dao;

import com.lld.problems.rideSharing2.models.Driver;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;

//Singleton
public class Drivers {

    private final HashMap<String, Driver> allDrivers;
    private final HashSet<String> availableDrivers;

    public Drivers() {
        allDrivers = new HashMap<>();
        availableDrivers = new HashSet<>();
    }

    public void addDriver(String name) {
        if(allDrivers.containsKey(name)) {
            System.out.println("Driver all ready exists");
            return;
        }

        allDrivers.put(name, new Driver(name));
        availableDrivers.add(name);
        System.out.println("Driver added");
    }

    public Driver getAvailableDriver() {
        Optional<String> optionalDriver = availableDrivers.stream().findAny();
        if(!optionalDriver.isPresent()) {
            System.out.println("No driver available");
            return null;
        }

        return allDrivers.get(optionalDriver.get());
    }

    public void updateDriverStatus(String name, boolean isRideAssigned) {
        if(!isRideAssigned) {
            availableDrivers.add(name);
        } else {
            availableDrivers.remove(name);
        }
    }
}
