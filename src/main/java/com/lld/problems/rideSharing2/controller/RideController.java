package com.lld.problems.rideSharing2.controller;

import com.lld.problems.rideSharing2.dao.Drivers;
import com.lld.problems.rideSharing2.dao.Riders;
import com.lld.problems.rideSharing2.models.Rider;
import com.lld.problems.rideSharing2.service.RideService;

public class RideController {
    private final RideService rideService;

    public RideController(Riders riders, Drivers drivers) {
        this.rideService = new RideService(riders, drivers);
    }

    public void createRideController(String riderName, int id, int origin, int destination, int numberOfSeats) {
        Rider rider = rideService.getRider(riderName);
        if(rideService.createRide(rider, id, origin, destination, numberOfSeats)) {
            System.out.println("Created ride");
        }
    }

    public void updateRideController(String riderName, int id, int origin, int destination, int numberOfSeats) {
        Rider rider = rideService.getRider(riderName);
        if(rideService.updateRide(rider, id, origin, destination, numberOfSeats)) {
            System.out.println("Updated ride");
        }
    }

    public void withDrawRideController(String riderName, int id) {
        Rider rider = rideService.getRider(riderName);
        if (rideService.withdrawRide(rider, id)) {
            System.out.println("Withdrawn ride");
        }
    }

    public void closeRideController(String riderName) {
        Rider rider = rideService.getRider(riderName);
        Integer cost = rideService.closeRide(rider);
        if(cost==null) {
            System.out.println("Cannot close ride");
            return;
        }

        System.out.println("Cost to be paid: " + cost);
    }

}
