package com.lld.problems.rideSharing2.service;

import com.lld.problems.rideSharing2.dao.Drivers;
import com.lld.problems.rideSharing2.dao.Riders;
import com.lld.problems.rideSharing2.models.Driver;
import com.lld.problems.rideSharing2.models.Ride;
import com.lld.problems.rideSharing2.models.RideStatus;
import com.lld.problems.rideSharing2.models.Rider;

public class RideService {
    private final Riders riders;
    private final Drivers drivers;

    public RideService(Riders riders, Drivers drivers) {
        this.riders = riders;
        this.drivers = drivers;
    }

    public Rider getRider(String userName) {
        Rider rider = riders.getRider(userName);
//        if(rider==null) {
//            throw error;
//            return null;
//        }

        return rider;
    }

    public boolean createRide(Rider rider, int id, int origin, int destination, int numberOfSeats) {
        if(rider.getPastRides().contains(id)) {
            // throw error
            System.out.println("Ride refers to past ride");
            return false;
        }

        if(origin==destination) {
            //throw error
            System.out.println("Origin and destination are same");
            return false;
        }

        Driver driver = drivers.getAvailableDriver();
        if(driver==null) {
            return false;
        }
        Ride ride = new Ride(id, origin, destination, numberOfSeats, driver);
        drivers.updateDriverStatus(driver.getName(), true);
        rider.setCurrentRide(ride);
        return true;
    }

    public boolean updateRide(Rider rider, int id, int origin, int destination, int numberOfSeats) {
        if(rider.getPastRides().contains(id)) {
            // throw error
            System.out.println("Cannot update past/completed ride");
            return false;
        }
        if(origin==destination) {
            //throw error
            System.out.println("Origin and destination are same");
            return false;
        }
        Ride ride;
        if(rider.getCurrentRide().getId()==id) {
            ride = rider.getCurrentRide();
            ride.setOrigin(origin);
            ride.setDestination(destination);
            ride.setNumberOfSeats(numberOfSeats);
        } else {
            createRide(rider, id, origin, destination, numberOfSeats);
        }
        return true;
    }

    public boolean withdrawRide(Rider rider,int id) {
        if(rider.getCurrentRide()==null ||
                rider.getCurrentRide().getId()!=id ||
                rider.getCurrentRide().getRideStatus() != RideStatus.CREATED){
            //throw error
            System.out.println("Cannot withdraw ride");
            return false;
        }

        Ride ride = rider.getCurrentRide();
        ride.setRideStatus(RideStatus.WITHDRAWN);
        rider.setCurrentRide(null);

        Driver driver = ride.getDriver();
        driver.setCurrentRide(null);
        drivers.updateDriverStatus(driver.getName(), false);
        return true;
    }

    public Integer closeRide(Rider rider) {
        if(rider.getCurrentRide()==null || rider.getCurrentRide().getRideStatus()!=RideStatus.CREATED) {
            //throw error
            System.out.println("Cannot close ride");
            return null;
        }

        Ride ride = rider.getCurrentRide();
        ride.setRideStatus(RideStatus.COMPLETED);
        rider.getPastRides().add(ride.getId());
        if(rider.getPastRides().size()>10) {
            rider.setPrivileged(true);
        }
        rider.setCurrentRide(null);

        Driver driver = ride.getDriver();
        driver.setCurrentRide(null);
        drivers.updateDriverStatus(driver.getName(), false);

        return ride.getCostOfRide(rider.isPrivileged());
    }
}
