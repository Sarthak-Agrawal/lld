package com.lld.problems.rideSharing2;

import com.lld.problems.rideSharing2.controller.RideController;
import com.lld.problems.rideSharing2.dao.Drivers;
import com.lld.problems.rideSharing2.dao.Riders;
import com.lld.problems.rideSharing2.models.Rider;

public class rideSharingMain {
    public static void main(String[] args) {
        Riders riders = new Riders();
        Drivers drivers = new Drivers();
        drivers.addDriver("abc");
        drivers.addDriver("def");

        Rider r1 = new Rider("Sarthak");
        Rider r2 = new Rider("Rajesh");
        riders.addRider(r1);
        riders.addRider(r2);

        RideController rideController = new RideController(riders, drivers);
        rideController.createRideController("Sarthak",1,50, 60, 1);
        rideController.updateRideController("Sarthak",1,50, 60, 2);
        rideController.closeRideController("Sarthak");
        System.out.println("Same ride should break");
        //Checking if same ride can be created
        rideController.createRideController("Sarthak",1,50, 60, 1);
        System.out.println("Cannot withdraw should come");
        //Checking whether the ride can be withdrawn
        rideController.withDrawRideController("Sarthak", 1);

        rideController.createRideController("Sarthak",2,40, 60, 1);
        rideController.withDrawRideController("Sarthak", 2);

        //Checking whether closing breaks when there is no ride
        rideController.closeRideController("Sarthak");
        rideController.createRideController("Sarthak",3,40, 60, 1);
        rideController.closeRideController("Sarthak");

        rideController.createRideController("Rajesh",1,50, 60, 1);
        rideController.createRideController("Sarthak",4,50, 60, 1);
        rideController.closeRideController("Sarthak");
        rideController.closeRideController("Rajesh");
    }
}
